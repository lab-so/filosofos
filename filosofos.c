#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <time.h>
#include <math.h>
#define NF 20

/*0: Pensando, 1: Comiendo
 * estos estados permiten determinar què esta haciendo el filosofo*/
int estado_Filosofo[NF];
sem_t semaforo_Filosofo[NF];

sem_t uso_servicios;

/* */
void pausa(int TiempoMax){
	srand(time(NULL));
	int nRandomNumber = rand()%TiempoMax;

	struct timespec reqPausa = {nRandomNumber, 0};
	nanosleep(&reqPausa, (struct timespec *) NULL);
}

void pensar(int i){
	printf("Filosofo %d Pensando\n", i);
	pausa(5);
}

void comer(int i){
	printf("Filosofo %d Comiendo\n", i);
	pausa(5);
}
/*si el estado del filosofo es 0 esta pensando, 
* y el filosofo vecino +1 y -1 es didtinto de 1 (comer)
* el filosofo cambia de estado
* y procede a comer
*/
void intentar_comer(int i){
	if (estado_Filosofo[i]== 0 && estado_Filosofo[(i+1) % NF ]!=1 && estado_Filosofo[(NF+i-1) % NF]!= 1){
		estado_Filosofo[i] = 1;
		sem_post(&semaforo_Filosofo[i]);
	}
}

void dejar_servicios(int i){
	sem_wait(&uso_servicios);
	estado_Filosofo[i] = 0;
	
	intentar_comer((i+1)%NF);
	intentar_comer((i+NF-1)%NF);
	sem_post(&uso_servicios);
}


void usar_servicios(int i){
	sem_wait(&uso_servicios);
	estado_Filosofo[i] = 1;
	intentar_comer(i);
	sem_post(&uso_servicios);
	sem_wait(&semaforo_Filosofo[i]);
}

/*esta funcion permite "turnar" las acciones de los filososfos*/
void *funcionFilosofo(void *arg){
	int *nf =  (int *)arg;
	/*directamente entrega los turnos a las acciones*/
	while(1){ 
		pensar(*nf);
		usar_servicios(*nf);
		comer(*nf);
		dejar_servicios(*nf);
	}
}

int main(int argc, char **argv){
	
	pthread_t filosofo[NF];
	int nf[NF];
	/*se inicializa los semaforos*/
	sem_init(&uso_servicios, 0, 1);
	for(int i=0; i< NF; i++){
		estado_Filosofo[i] = 0;
		sem_init(&semaforo_Filosofo[i], 0, 0); /*todos comienzan pensando*/
	}

	for(int i=0; i < NF; i++){
		nf[i] = i;
		pthread_create(&filosofo[i], NULL, funcionFilosofo, (void *)&nf[i]);
	}

	for(int i=0; i < NF; i++)
	pthread_join(filosofo[i], NULL);
 
}
